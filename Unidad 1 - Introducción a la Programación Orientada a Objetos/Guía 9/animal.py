from organismo import Organismo

class Animal(Organismo):

    def __init__(self, nombre, edad, numero_patas):
        super().__init__(nombre, edad)
        self.__numero_patas = numero_patas
        self.infectado = False 

    # Método para que el animal se mueva.
    def moverse(self):
        return f"{self.get_nombre()} se está moviendo."

    # Método para que el animal haga un sonido.
    def hacer_sonido(self):
        return f"{self.get_nombre()} está haciendo un sonido."

    # Método para realizar acciones diarias del animal
    def acciones_diarias(self):
        super().acciones_diarias() 

    # Método para verificar si el animal está infectado.
    def esta_infectado(self):
        return self.infectado

    # Método para curar al animal.
    def curar(self):
        self.infectado = False