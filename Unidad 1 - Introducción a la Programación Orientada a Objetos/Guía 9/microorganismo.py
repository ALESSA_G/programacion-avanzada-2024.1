from organismo import Organismo  
import random  

class Microorganismo(Organismo):  

    def __init__(self, nombre, edad, tipo):
        super().__init__(nombre, edad)  
        self.__tipo = tipo  
        self.infectado = False  

    # Método para que el microorganismo se reproduzca.
    def reproducirse(self):
        return f"{self.get_nombre()} se está reproduciendo."

    # Método para infectar.
    def infectar(self): 
        return f"{self.get_nombre()} está infectando."

    def get_tipo(self):  
        return self.__tipo

    def set_tipo(self, tipo):
        self.__tipo = tipo
    
    # Método para la mutación del microorganismo.
    def mutar(self, especies_en_biota):  
        if self.esta_infectado() and random.random() < 0.6: # Mutación ocurre con 60% de probabilidad.
            for especie in especies_en_biota:
                if especie != self and random.random() < 0.6: # Contagio a otras especies con 60% de probabilidad.
                    especie.infectado = True 
                    print(f"{self.get_nombre()} ha mutado y ha infectado a {especie.get_nombre()}.")

    # Método para verificar si el microorganismo está infectado.
    def esta_infectado(self): 
        return self.infectado

    # Método para curar al microorganismo.
    def curar(self): 
        self.infectado = False  
