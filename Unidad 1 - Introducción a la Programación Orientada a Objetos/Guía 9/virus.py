class Virus:
    def __init__(self, nombre): 
        self.nombre = nombre 

    # Método para infectar una especie.
    def infectar(self, especie):
        print(f"{self.nombre} infecta a {especie.get_nombre()}")
        especie.infectado = True  # Marcar la especie como infectada.
