class Bacteria:
    
    def __init__(self, nombre):
        self.nombre = nombre

    # Método para infectar a una especie.
    def infectar(self, especie):
        # Verificamos si la especie no está infectada.
        if not especie.esta_infectado(): 
            # Imprimimos un mensaje indicando la infección.
            print(f"{self.nombre} infecta a {especie.get_nombre()}")
            # Marcamos la especie como infectada.
            especie.infectado = True
        else:
            # Si la especie ya está infectada, imprimimos un mensaje indicándolo.
            print(f"{especie.get_nombre()} ya está infectado.")
