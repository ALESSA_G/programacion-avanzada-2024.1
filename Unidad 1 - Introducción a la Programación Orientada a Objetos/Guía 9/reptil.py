from animal import Animal

class Reptil(Animal): 

    # Método para simular el cambio de piel de un reptil. 
    def cambiar_de_piel(self):  
        return f"{self.get_nombre()} está cambiando de piel."
    
    # Método que representa las acciones diarias de un reptil.
    def acciones_diarias(self):  
        print(f"{self.get_nombre()} está tomando el sol.")
    
    # Método para verificar si el reptil está infectado.
    def esta_infectado(self):  
        return self.infectado

    # Método para curar la infección en un reptil.
    def curar(self):  
        self.infectado = False
