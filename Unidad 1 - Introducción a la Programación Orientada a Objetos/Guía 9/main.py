# Gabriela Sepúlveda Rojas - Programación Avanzada - 2024.1#

from ave import Ave
from mamifero import Mamifero
from reptil import Reptil
from flor import Flor
from arbol import Arbol
from microorganismo import Microorganismo
from celula import Celula
from virus import Virus
from bacteria import Bacteria
import time
import random

# Algunas especies con sus respectivas características.
aguila = Ave("Águila", 5, 2)
tigre = Mamifero("Tigre", 7, 4)
serpiente = Reptil("Serpiente", 3, 0)
rosa = Flor("Rosa", 1, "rosa")
roble = Arbol("Roble", 10, "roble")
bacteria = Microorganismo("Bacteria", 0.5, "bacteria")
cancer = Celula("Cancer", 0.1, "cancer")

# Lista para almacenar todas las especies.
especies_en_biota = [aguila, tigre, serpiente, rosa, roble, bacteria, cancer]

def main():
    dia = 1
    while dia <= 365: # Contador de un año en Biótica.
        # Separador de días.
        print("-" * 20)
        print(f"Nuevo día en Biótica: Día {dia}")
        print("-" * 20)

        # Simular múltiples interacciones aleatorias.
        for _ in range(5):
            interaccion_aleatoria()

        # Realiza acciones diarias para cada especie.
        for especie in especies_en_biota:
            especie.acciones_diarias()
            especie.convertirse_en_zombie()
            # Llamar al método mutar() solo si la especie es un microorganismo.
            if isinstance(especie, Microorganismo):
                especie.mutar(especies_en_biota)

        # Distribuye enfermedades.
        distribuir_enfermedades()

        # Fabrica el antídoto después de 2 días y lo aplica.
        if dia % 2 == 0: 
            fabricar_antidoto()
            aplicar_antidoto()

        # Espera un segundo antes de continuar con el próximo día.
        time.sleep(1)
        dia += 1

# Función para simular una interacción aleatoria entre dos organismos.
def interaccion_aleatoria():
    organismo1, organismo2 = random.sample(especies_en_biota, 2)  # Seleccionar dos organismos aleatorios de la lista.
    print(f"\nInteracción entre {organismo1.get_nombre()} y {organismo2.get_nombre()}:")

    if isinstance(organismo1, Reptil) and isinstance(organismo2, Ave):
        print(f"{organismo1.get_nombre()} se come a {organismo2.get_nombre()}.")
    elif isinstance(organismo1, Ave) and isinstance(organismo2, Reptil):
        print(f"{organismo2.get_nombre()} se come a {organismo1.get_nombre()}.")
    elif isinstance(organismo1, Arbol) and isinstance(organismo2, Microorganismo):
        print(f"{organismo2.get_nombre()} infecta a {organismo1.get_nombre()}.")
    elif isinstance(organismo1, Flor) and isinstance(organismo2, Microorganismo):
        print(f"{organismo2.get_nombre()} infecta a {organismo1.get_nombre()}.")
    elif isinstance(organismo1, Microorganismo) and isinstance(organismo2, Arbol):
        print(f"{organismo1.get_nombre()} infecta a {organismo2.get_nombre()}.")
    elif isinstance(organismo1, Celula) and isinstance(organismo2, Microorganismo):
        print(f"{organismo2.get_nombre()} infecta a {organismo1.get_nombre()}.")
    elif isinstance(organismo1, Microorganismo) and isinstance(organismo2, Celula):
        print(f"{organismo1.get_nombre()} infecta a {organismo2.get_nombre()}.")
    else:
        print(f"No hay interacción entre {organismo1.get_nombre()} y {organismo2.get_nombre()}.")

# Distribuye virus y bacterias en las comunidades.
def distribuir_enfermedades():
    for especie in especies_en_biota:
        # 60% de probabilidad de infectar con virus o bacterias.
        if random.random() < 0.6: 
            virus = Virus("Virus Aleatorio")
            virus.infectar(especie)
        if random.random() < 0.6:
            bacteria = Bacteria("Bacteria Aleatoria")
            bacteria.infectar(especie)

def fabricar_antidoto():
    print("Iniciando la fabricación del antídoto...")
    time.sleep(2)  # Proceso de fabricación de 2 segundos.
    print("El antídoto ha sido fabricado con éxito.")

def aplicar_antidoto():
    infectados = [especie for especie in especies_en_biota if especie.esta_infectado()]
    if infectados:
        organismo_curado = random.choice(infectados)
        organismo_curado.curar()
        print(f"Se ha utilizado el antídoto en {organismo_curado.get_nombre()} para curar la infección.")
    else:
        print("No hay organismos infectados para curar con el antídoto.")

if __name__ == "__main__":
    main()