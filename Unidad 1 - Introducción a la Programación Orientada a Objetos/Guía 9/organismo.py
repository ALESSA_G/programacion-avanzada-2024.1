import random  

class Organismo:
    def __init__(self, nombre, edad):
        self.__nombre = nombre  
        self.__edad = edad  
        self.infectado = False  

    def get_nombre(self): 
        return self.__nombre
    
    # Método para hacer que el organismo crezca.
    def crecer(self): 
        self.__edad += 1  # Incremento de la edad.
        print(f"{self.__nombre} creció y ahora tiene {self.__edad} años.")

    # Método para que el organismo se reproduzca.
    def reproducirse(self):
        print(f"{self.__nombre} se está reproduciendo.")

    # Método para simular la muerte del organismo.
    def morir(self):
        print(f"{self.__nombre} murió.")

    # Método para las acciones diarias del organismo.
    def acciones_diarias(self): 
        if random.random() < 0.45: # 45% de probabilidad de convertirse en zombie.
            self.convertirse_en_zombie()
        elif random.random() < 0.25: # 25% de probabilidad de morir.
            self.morir()
        else:
            print(f"{self.get_nombre()} está llevando a cabo sus actividades diarias.")

    # Método para verificar si el organismo está infectado.
    def esta_infectado(self): 
        return self.infectado
    
    # Método para que el organismo se convierta en zombie.
    def convertirse_en_zombie(self): 
        if self.esta_infectado():  # Verificamos si el organismo está infectado.
            print(f"{self.get_nombre()} se ha convertido en un zombie.")
