from planta import Planta

class Arbol(Planta):
    
    # Método para que al árbol le crezcan frutos.
    def crecer_fruto(self):
        return f"{self.get_nombre()} le están creciendo fruto."
    
    # Método para realizar acciones diarias del árbol.
    def acciones_diarias(self):
        print(f"{self.get_nombre()} está creciendo.")

    # Método para verificar si el árbol está infectado.
    def esta_infectado(self):
        return self.infectado

    # Método para curar al árbol.
    def curar(self):
        self.infectado = False