from animal import Animal

class Ave(Animal):

    # Método para que el ave vuele.
    def volar(self):
        return f"{self.get_nombre()} está volando."

    # Método para que el ave cante.
    def cantar(self):
        return f"{self.get_nombre()} está cantando."
    
    # Método para realizar acciones diarias del ave.
    def acciones_diarias(self):
        print(f"{self.get_nombre()} está volando.")

    # Método para verificar si el ave está infectada.
    def esta_infectado(self):
        return self.infectado

    # Método para curar al ave.
    def curar(self):
        self.infectado = False