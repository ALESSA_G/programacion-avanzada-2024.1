from animal import Animal

class Mamifero(Animal):

    # Método para que el mamífero pueda lactar.
    def lactar(self):
        return f"{self.get_nombre()} está lactando."
    
    # Método que define las acciones diarias del mamífero.
    def acciones_diarias(self):
        print(f"{self.get_nombre()} está buscando alimento.")

    # Método para verificar si el mamífero está infectado.
    def esta_infectado(self):
        return self.infectado

    # Método para curar al mamífero.
    def curar(self): 
        self.infectado = False
