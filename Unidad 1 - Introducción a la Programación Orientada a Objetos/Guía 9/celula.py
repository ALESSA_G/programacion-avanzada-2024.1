from organismo import Organismo
import random

class Celula(Organismo):
    
    def __init__(self, nombre, edad, tipo):
        super().__init__(nombre, edad)
        self.__tipo = tipo
        self.infectado = False

    # Método para la división celular.
    def dividirse(self):
        return f"{self.get_nombre()} se está dividiendo."

    def get_tipo(self):
        return self.__tipo

    def set_tipo(self, tipo):
        self.__tipo = tipo

    # Método para las acciones diarias de la célula.
    def acciones_diarias(self):
        # Se simula la posibilidad de convertirse en zombie o morir.
        if random.random() < 0.45:  # 45% de probabilidad de convertirse en zombie.
            self.convertirse_en_zombie()
        elif random.random() < 0.25:  # 25% de probabilidad de morir.
            self.morir()
        else:
            # Si no se convierte en zombie ni muere, simplemente se está dividiendo.
            print(f"{self.get_nombre()} está dividiéndose.")

    # Método para verificar si la célula está infectada.
    def esta_infectado(self):
        return self.infectado

    # Método para curar la célula.
    def curar(self):
        self.infectado = False
