from organismo import Organismo 

class Planta(Organismo): 
    def __init__(self, nombre, edad, tipo):
        super().__init__(nombre, edad)  
        self.__tipo = tipo 

    # Método para simular la fotosíntesis de la planta.
    def fotosintesis(self): 
        return f"{self.get_nombre()} está realizando fotosíntesis."
    
    # Método para que la planta se reproduzca.
    def reproducirse(self): 
        return f"{self.get_nombre()} está reproduciéndose."

    def getTipo(self):
        return self.__tipo

    def setTipo(self, tipo):
        self.__tipo = tipo