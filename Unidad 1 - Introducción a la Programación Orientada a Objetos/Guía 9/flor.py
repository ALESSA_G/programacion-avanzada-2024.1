from planta import Planta

class Flor(Planta):
    
    # Método para la polinización de la flor.
    def polinizar(self):
        return f"{self.get_nombre()} está polinizando."

    # Método para las acciones diarias de la flor.
    def acciones_diarias(self):
        print(f"{self.get_nombre()} está realizando fotosíntesis.")

    # Método para verificar si la flor está infectada.
    def esta_infectado(self):
        return self.infectado

    # Método para curar la flor.
    def curar(self):
        self.infectado = False