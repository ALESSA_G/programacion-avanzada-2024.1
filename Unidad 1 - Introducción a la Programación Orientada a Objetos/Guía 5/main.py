from paciente import Paciente

# Datos del paciente, siendo estos ingresados por el usuario.
def obtener_datos_paciente():
    try:
        nombre = input("- Ingrese el nombre del paciente: ")
        edad = int(input("- Ingrese la edad del paciente: "))
        genero = input("- Ingrese el género del paciente: ")
        antecedentes_medicos = input("- Ingrese los antecedentes médicos del paciente: ")
        return nombre, edad, genero, antecedentes_medicos
    
    except ValueError:
        print("La edad debe ser un número entero.") # Tomar en cuenta que el valor de la edad ingresada sea correcta.
        return None, None, None, None

def main():
    print("¡Bienvenido al sistema de gestión médica!\n")

    # Obtención de los datos del paciente.
    nombre, edad, genero, antecedentes_medicos = obtener_datos_paciente()
    if None in (edad, genero, antecedentes_medicos):
        print("* Hubo un problema al ingresar los datos del paciente, intente nuevamente. *") # Envíar error en caso de que los parámetros no sean correctos.
        return
    try: 
        paciente1 = Paciente(nombre, edad, genero, antecedentes_medicos)
    except ValueError as e:
        print("Error:", e)
        return

    # Creación de un "paciente1" con los datos ingresados.
    print("\n* Datos del paciente ingresados *")
    print("- Nombre:", paciente1.nombre)
    print("- Edad:", paciente1.edad)
    print("- Género:", paciente1.genero)
    print("- Antecedentes médicos:", paciente1.antecedentes_medicos)

    # Agrega la atención del paciente al historial clínico.
    try:
        print("\n* Atención médica *")
        fecha = input("- Ingrese la fecha de la atención médica: ")
        motivo = input("- Ingrese el motivo de la atención médica: ")
        diagnostico = input("- Ingrese el diagnóstico de la atención médica: ")
        recomendacion = input("- Ingrese la recomendación de la atención médica: ")
        paciente1.agregar_atencion(fecha, motivo, diagnostico, recomendacion)
        print("\n¡Atención médica registrada con éxito!\n")
    except Exception as e: 
        print("Error:", e)

    # Impresión del historial clínico.
    historial = paciente1.obtener_historial_atencion()
    if historial:
        print("\n* Historial de atención del paciente *")
        for fecha, datos in historial.items():
            print("\n- Fecha:", fecha)
            print("- Motivo de consulta:", datos['motivo'])
            print("- Diagnóstico:", datos['diagnostico'])
            print("- Recomendación:", datos['recomendacion'])
    else:
        print("El paciente no tiene historial de atención médica.")

if __name__ == "__main__":
    main()