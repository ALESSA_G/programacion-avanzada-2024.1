class Paciente:
    
    def __init__(self, nombre, edad, genero, antecedentes_medicos):
        self.nombre = nombre
        self.edad = self.validar_edad(edad)
        self.genero = genero
        self.antecedentes_medicos = antecedentes_medicos
        self.historial_atencion = {}

    def validar_edad(self, edad): #Valida que la entreda sea un número entero positivo, de lo contrario retornará None.
        
        try:
            edad = int(edad)
            if edad < 0:
                raise ValueError("La edad debe ser un número entero positivo.")
            return edad
        except ValueError:
            print("Error: La edad debe ser un número entero positivo.")
            return None

    def agregar_atencion(self, fecha, motivo, diagnostico, recomendacion):
        
        self.historial_atencion[fecha] = {
            'motivo': motivo,
            'diagnostico': diagnostico,
            'recomendacion': recomendacion
        }

    def obtener_historial_atencion(self):
        return self.historial_atencion