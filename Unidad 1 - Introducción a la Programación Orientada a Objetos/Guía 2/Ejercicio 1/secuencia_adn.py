class SecuenciaADN:
    
    def __init__(self, identificador, secuencia_adn, especie):
        self.__identificador = identificador
        self.__secuencia_adn = secuencia_adn
        self.__especie = especie

    #Método Getter
    def obtener_identificador(self):
        return self.__identificador

    def obtener_secuencia_adn(self):
        return self.__secuencia_adn

    def obtener_especie(self):
        return self.__especie

    #Método Setter
    def establecer_identificador(self, identificador):
        self.__identificador = identificador

    def establecer_secuencia_adn(self, secuencia_adn):
        self.__secuencia_adn = secuencia_adn

    def establecer_especie(self, especie):
        self.__especie = especie

    #Cálculo de longitud de la secuencia de ADN#
    def calcular_longitud_secuencia(self):
        return len(self.__secuencia_adn)

    #Transcripción de la secuencia (ADN - ARN)
    def transcribir_secuencia_a_arn(self):
        return self.__secuencia_adn.replace('T', 'U')
