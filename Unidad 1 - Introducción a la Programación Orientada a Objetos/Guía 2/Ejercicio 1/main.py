from lector import leer_archivo

def main():
    archivo = 'datos_ejercicio1.txt'
    secuencias_adn = leer_archivo(archivo)
    print(" *RESULTADOS DEL ANÁLISIS DE SECUENCIA DE ADN:")
    for secuencia_adn in secuencias_adn:
        #Mostrar los resultados actualizados del análisis
        print("- Identificador:", secuencia_adn.obtener_identificador())
        print("- Secuencia de ADN:", secuencia_adn.obtener_secuencia_adn())
        print("- Especie:", secuencia_adn.obtener_especie())
        print("- Longitud de la secuencia de ADN:", secuencia_adn.calcular_longitud_secuencia())
        print("- Transcripción a ARN:", secuencia_adn.transcribir_secuencia_a_arn())
        print()

if __name__ == "__main__":
    main()
