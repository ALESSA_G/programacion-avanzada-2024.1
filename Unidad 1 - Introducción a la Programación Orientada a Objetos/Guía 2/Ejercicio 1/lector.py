from secuencia_adn import SecuenciaADN

def leer_archivo(nombre_archivo):
    #Almacena las secuencias del archivo
    secuencias_adn = []
    with open(nombre_archivo, 'r') as archivo:
        #Ignora el encabezado del archivo, ya que este contiene las carcaterísticas evaluadas.
        next(archivo) 
        for linea in archivo:
            secciones = linea.strip().split(',') #Observa la separación de datos por comas.
            if len(secciones) == 3:
                identificador, secuencia_adn, especie = secciones
                secuencia_adn_objeto = SecuenciaADN(identificador, secuencia_adn, especie)
                secuencias_adn.append(secuencia_adn_objeto)
    return secuencias_adn