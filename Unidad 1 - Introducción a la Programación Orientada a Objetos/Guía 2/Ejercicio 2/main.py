from mutaciongenetica import MutacionGenetica
from lector import LectorMutaciones

def main():
    # Leer las mutaciones desde el archivo
    mutaciones = LectorMutaciones.leer_mutaciones_desde_archivo('datos_ejercicio2.txt')

    # Mostrar información de todas las mutaciones y verificar si son puntuales
    for mutacion in mutaciones:
        print("\nInformación de la mutación:")
        mutacion.mostrar_informacion()
        if mutacion.es_mutacion_puntual():
            print("La mutación es puntual.")
        else:
            print("La mutación no es puntual.")

if __name__ == "__main__":
    main()