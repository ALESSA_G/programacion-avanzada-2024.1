class MutacionGenetica:

    def __init__(self, identificador, descripcion, region_genomica, especie):
        self.__identificador = identificador
        self.__descripcion = descripcion
        self.__region_genomica = region_genomica
        self.__especie = especie

    # Métodos Setter
    def set_identificador(self, identificador):
        self.__identificador = identificador

    def set_descripcion(self, descripcion):
        self.__descripcion = descripcion

    def set_region_genomica(self, region_genomica):
        self.__region_genomica = region_genomica

    def set_especie(self, especie):
        self.__especie = especie

    # Métodos Getter
    def get_identificador(self):
        return self.__identificador

    def get_descripcion(self):
        return self.__descripcion

    def get_region_genomica(self):
        return self.__region_genomica

    def get_especie(self):
        return self.__especie

    # Determina si la mutación es puntual
    def es_mutacion_puntual(self):
        return "sustitución" in self.__descripcion.lower()

    # Muestra la información de la mutación
    def mostrar_informacion(self):
        print("- Identificador:", self.__identificador)
        print("- Descripción de la mutación:", self.__descripcion)
        print("- Región genómica afectada:", self.__region_genomica)
        print("- Especie asociada:", self.__especie)
