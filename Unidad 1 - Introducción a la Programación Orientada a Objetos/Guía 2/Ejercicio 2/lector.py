from mutaciongenetica import MutacionGenetica

class LectorMutaciones:
    def leer_mutaciones_desde_archivo(archivo):
        mutaciones = []
        with open(archivo, 'r', encoding='utf-8') as f:
            next(f)  # Saltar la primera línea que contiene los encabezados
            for linea in f:
                datos = linea.strip().split(',')
                identificador = datos[0]
                descripcion = datos[1]
                region_genomica = ','.join(datos[2:-1])  
                especie = datos[-1] 
                mutacion = MutacionGenetica(identificador, descripcion, region_genomica, especie)
                mutaciones.append(mutacion)
        return mutaciones
