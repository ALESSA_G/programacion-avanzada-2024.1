from ave import Ave
from mamifero import Mamifero
from reptil import Reptil
from flor import Flor
from arbol import Arbol
from microorganismo import Microorganismo
from celula import Celula

def main():
    # Organismos de ejemplo para el ejercicio, eligiendo uno por grupo.
    organismos = [
        Ave("Águila", 5, 2),
        Mamifero("Tigre", 7, 4),
        Reptil("Serpiente", 3, 0),
        Flor("Rosa", 1, "rosa"),
        Arbol("Roble", 10, "roble"),
        Microorganismo("Bacteria", 0.5, "bacteria"),
        Celula("Cancer", 0.1, "cancer")
    ]

    # Simulación de interacciones entre organismos
    print("Simulación de interacciones entre organismos:")
    print("-" * 50)
    for organismo1 in organismos:
        for organismo2 in organismos:
            if organismo1 != organismo2: # Evitamos la interacción de un organismo con sí mismo.
                interaccion_entre(organismo1, organismo2)
    print("-" * 50)

#Posibles interacciones ficticias entre organismos.
def interaccion_entre(organismo1, organismo2):

    if isinstance(organismo1, Ave) and isinstance(organismo2, Arbol):
        print(f"{organismo1.get_nombre()} está volando hacia {organismo2.get_nombre()}")
    elif isinstance(organismo1, Mamifero) and isinstance(organismo2, Flor):
        print(f"{organismo1.get_nombre()} está oliendo {organismo2.get_nombre()}")
    elif isinstance(organismo1, Microorganismo) and isinstance(organismo2, Reptil):
        print(f"{organismo1.get_nombre()} está infectando a {organismo2.get_nombre()}")  
    elif isinstance(organismo1, Celula) and isinstance(organismo2, Ave):
        print(f"{organismo1.get_nombre()} está dividiéndose en {organismo2.get_nombre()}")

if __name__ == "__main__":
    main()