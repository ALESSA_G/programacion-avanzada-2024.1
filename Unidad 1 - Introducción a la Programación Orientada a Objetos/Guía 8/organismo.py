class Organismo:

    def __init__(self, nombre, edad):
        self.__nombre = nombre
        self.__edad = edad

    def nacer(self):
        print(f"{self.__nombre} ha nacido.")

    def crecer(self):
        self.__edad += 1
        print(f"{self.__nombre} creció y ahora tiene {self.__edad} años.")

    def reproducirse(self):
        print(f"{self.__nombre} se está reproduciendo.")

    def morir(self):
        print(f"{self.__nombre} murió.")

    def get_nombre(self):
        return self.__nombre

    def set_nombre(self, nombre):
        self.__nombre = nombre

    def get_edad(self):
        return self.__edad

    def set_edad(self, edad):
        self.__edad = edad
