from planta import Planta

class Arbol(Planta):
    
    def crecer_fruto(self):
        return f"{self.get_nombre()} está creciendo fruto."