from planta import Planta

class Flor(Planta):

    def polinizar(self):
        return f"{self.get_nombre()} está polinizando."
