from organismo import Organismo

class Microorganismo(Organismo):

    def __init__(self, nombre, edad, tipo):
        super().__init__(nombre, edad)
        self.__tipo = tipo

    def reproducirse(self):
        return f"{self.get_nombre()} se está reproduciendo."

    def infectar(self):
        return f"{self.get_nombre()} está infectando."

    def get_tipo(self):
        return self.__tipo

    def set_tipo(self, tipo):
        self.__tipo = tipo
