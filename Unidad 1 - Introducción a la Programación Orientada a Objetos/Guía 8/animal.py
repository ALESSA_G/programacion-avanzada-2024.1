from organismo import Organismo

class Animal(Organismo):

    def __init__(self, nombre, edad, numero_patas):
        super().__init__(nombre, edad)
        self.__numero_patas = numero_patas

    def moverse(self):
        return f"{self.get_nombre()} se está moviendo."

    def hacer_sonido(self):
        return f"{self.get_nombre()} está haciendo un sonido."

    def get_numeropatas(self):
        return self.__numero_patas

    def set_numeropatas(self, numero_patas):
        self.__numero_patas = numero_patas
