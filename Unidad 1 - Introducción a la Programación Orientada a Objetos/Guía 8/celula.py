from organismo import Organismo

class Celula(Organismo):
    def __init__(self, nombre, edad, tipo):
        super().__init__(nombre, edad)
        self._tipo = tipo

    def dividirse(self):
        return f"{self._nombre} se está dividiendo."

    def get_tipo(self):
        return self._tipo

    def set_tipo(self, tipo):
        self._tipo = tipo
