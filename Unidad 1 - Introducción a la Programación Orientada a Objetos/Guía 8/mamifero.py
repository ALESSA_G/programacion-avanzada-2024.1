from animal import Animal

class Mamifero(Animal):

    def lactar(self):
        return f"{self.get_nombre()} está lactando."