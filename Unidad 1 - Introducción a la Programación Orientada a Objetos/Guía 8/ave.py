from animal import Animal

class Ave(Animal):

    def volar(self):
        return f"{self.get_nombre()} está volando."

    def cantar(self):
        return f"{self.get_nombre()} está cantando."
