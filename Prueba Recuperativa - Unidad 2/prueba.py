import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
import json

class MainWindow(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="Consumo Energético de Electrodomésticos")
        self.set_default_size(100, 250)

        # Creación del grid para organizar los elementos.
        self.grid = Gtk.Grid()
        self.add(self.grid)

        # Etiqueta y entrada para el nombre del electrodoméstico proporcionado por el usuario.
        self.label_nombre = Gtk.Label(label="Nombre del electrodoméstico:")
        self.grid.attach(self.label_nombre, 0, 0, 1, 1)

        self.entry_nombre = Gtk.Entry()
        self.entry_nombre.set_placeholder_text("Ej: Refrigerador, Lavadora, etc.")
        self.grid.attach(self.entry_nombre, 1, 0, 1, 1)

        # Etiqueta y entrada para el consumo diario en kWh del artefacto.
        self.label_consumo = Gtk.Label(label="Consumo diario en kWh:")
        self.grid.attach(self.label_consumo, 0, 1, 1, 1)

        self.entry_consumo = Gtk.Entry()
        self.entry_consumo.set_placeholder_text("Ej: 2.6")
        self.grid.attach(self.entry_consumo, 1, 1, 1, 1)

        # Botón para agregar el electrodoméstico a la lista.
        self.button_agregar = Gtk.Button(label="Agregar Electrodoméstico")
        self.button_agregar.connect("clicked", self.on_button_agregar_clicked)
        self.grid.attach(self.button_agregar, 0, 2, 2, 1)

        # Botón para calcular el consumo total de los artefactos de la lista.
        self.button_calcular = Gtk.Button(label="Calcular Consumo Total")
        self.button_calcular.connect("clicked", self.on_button_calcular_clicked)
        self.grid.attach(self.button_calcular, 0, 3, 2, 1)

        # Botón para recomendar ahorro de energía, en relación al gasto de los artefactos de la lista.
        self.button_recomendar = Gtk.Button(label="Recomendar Ahorro de Energía")
        self.button_recomendar.connect("clicked", self.on_button_recomendar_clicked)
        self.grid.attach(self.button_recomendar, 0, 4, 2, 1)

        # Área para mostrar la lista de electrodomésticos y el consumo en kWh.
        self.liststore = Gtk.ListStore(str, float)
        self.treeview = Gtk.TreeView(model=self.liststore)

        renderer_text = Gtk.CellRendererText()
        column1 = Gtk.TreeViewColumn("Electrodoméstico", renderer_text, text=0)
        self.treeview.append_column(column1)

        column2 = Gtk.TreeViewColumn("Consumo diario (kWh)", renderer_text, text=1)
        self.treeview.append_column(column2)

        self.grid.attach(self.treeview, 0, 5, 2, 1)

        # Etiqueta para mostrar el consumo total de los artefactos.
        self.label_consumo_total = Gtk.Label(label="")
        self.grid.attach(self.label_consumo_total, 0, 6, 2, 1)

        # Etiqueta para mostrar la recomendación de ahorro de energía de los artefactos.
        self.label_recomendacion = Gtk.Label(label="")
        self.grid.attach(self.label_recomendacion, 0, 7, 2, 1)

        self.cargar_datos_desde_json()

    def on_button_agregar_clicked(self, button):
        # Obtener nombre y consumo del artefactos por el usuario.
        nombre = self.entry_nombre.get_text()
        consumo_str = self.entry_consumo.get_text()

        try:
            consumo = float(consumo_str)
        except ValueError:
            # Error si el consumo no es un número válido.
            return

        # Agregar a la lista y guardar en el archivo JSON.
        self.liststore.append([nombre, consumo])
        self.guardar_datos(nombre, consumo)

    def cargar_datos_desde_json(self):
        try:
            # Abrir el archivo JSON y cargar los datos obtenidos.
            with open('datos_electrodomesticos.json', 'r') as file:
                for line in file:
                    data = json.loads(line)
                    nombre = data['nombre']
                    consumo = data['consumo_diario_kWh']
                    self.liststore.append([nombre, consumo])
        except IOError:
            # Error si el archivo no existe o no se puede abrir.
            pass

    def guardar_datos(self, nombre, consumo):
        # Guardar en un archivo JSON los datos ingresados por el usuario.
        data = {"nombre": nombre, "kWh_diario": consumo}

        try:
            with open('datos_de_electrodomesticos.json', 'a') as file:
                json.dump(data, file)
                file.write('\n')
        except IOError:
            # Error si hay problemas al escribir en el archivo.
            pass

    def on_button_calcular_clicked(self, button):
        # Calcular el consumo total de todos los electrodomésticos.
        consumo_total = 0.0

        for row in self.liststore:
            consumo_total += row[1]
        self.label_consumo_total.set_label(f"Consumo total: {consumo_total} kWh")

    def on_button_recomendar_clicked(self, button):
        # Recomendación de ahorro de energía.
        self.recomendar_ahorro_energia()

    def recomendar_ahorro_energia(self):
        consumos = [(row[0], row[1]) for row in self.liststore]
        consumos_ordenados = sorted(consumos, key=lambda x: x[1], reverse=True)

        if len(consumos_ordenados) > 0:
            electrodomestico_mas_alto = consumos_ordenados[0][0]
            recomendacion = f"Recomendación: Reducir uso de {electrodomestico_mas_alto} para ahorrar energía."
            self.label_recomendacion.set_label(recomendacion)

win = MainWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()
