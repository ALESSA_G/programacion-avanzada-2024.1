from enfermedad import Enfermedad
from comunidad import Comunidad
from simulador import Simulador

def main():
    covid = Enfermedad(infeccion_probable=0.3, promedio_pasos=18)
    talca = Comunidad(num_ciudadanos=19, promedio_conexion_fisica=0.8, enfermedad=covid, num_infectados=2, probabilidad_conexion_fisica=0.5)
    simulador = Simulador(talca)
    simulador.run(pasos=20)

if __name__ == "__main__":
    main()
