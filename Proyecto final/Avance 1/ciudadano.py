import random

class Ciudadano:
    def __init__(self, _id, nombre, apellido, familia, comunidad):
        self.id = _id
        self.nombre = nombre
        self.apellido = apellido
        self.familia = familia
        self.comunidad = comunidad
        self.enfermedad = None
        self.estado = True

    def tiene_contacto_fisico(self, otro_ciudadano):
        if self == otro_ciudadano:
            return False 
        if self.familia == otro_ciudadano.familia:
            return True  # Miembros de la misma familia tienen contacto físico estrecho
        else:
            return random.random() < self.comunidad.probabilidad_conexion_fisica

    def infectarse(self):
        if self.enfermedad and self.estado:
            self.estado = False  # El ciudadano se infecta

    def __repr__(self):
        return f"Ciudadano {self.nombre} {self.apellido}"
