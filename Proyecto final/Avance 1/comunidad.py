import csv
import os
import random
from ciudadano import Ciudadano

class Comunidad:
    def __init__(self, num_ciudadanos, promedio_conexion_fisica, enfermedad, num_infectados, probabilidad_conexion_fisica):
        self.num_ciudadanos = num_ciudadanos
        self.promedio_conexion_fisica = promedio_conexion_fisica
        self.enfermedad = enfermedad
        self.num_infectados = num_infectados
        self.probabilidad_conexion_fisica = probabilidad_conexion_fisica
        self.ciudadanos = []
        self.cargar_ciudadanos_desde_csv()

    def cargar_ciudadanos_desde_csv(self):
        try:
            archivo_personas = os.path.join(os.path.dirname(__file__), 'personas.csv')
            with open(archivo_personas, mode='r', newline='') as file:
                reader = csv.DictReader(file)
                for index, row in enumerate(reader):
                    nombre = row['nombre']
                    apellido = row['apellido']
                    familia = row['familia']
                    self.ciudadanos.append(Ciudadano(index, nombre, apellido, familia, self))
                    
                    # Salir del bucle si alcanza el número máximo de ciudadanos
                    if len(self.ciudadanos) >= self.num_ciudadanos:
                        break
            
            # Verifica si el número de ciudadanos supera el número total especificado
            if len(self.ciudadanos) > self.num_ciudadanos:
                self.ciudadanos = self.ciudadanos[:self.num_ciudadanos]

        except FileNotFoundError:
            print(f"Error: No se pudo encontrar el archivo {archivo_personas}")
        except Exception as e:
            print(f"Error al cargar ciudadanos desde CSV: {e}")

    def infectar_ciudadanos_iniciales(self):
        infectados_iniciales = random.sample(self.ciudadanos, self.num_infectados)
        for ciudadano in infectados_iniciales:
            ciudadano.enfermedad = self.enfermedad
            ciudadano.estado = False  # Marcar como enfermo

    def simular_paso(self):
        nuevos_infectados = []
        nuevos_recuperados = []
        for ciudadano in self.ciudadanos:
            if ciudadano.estado:  # Solo procesar ciudadanos sanos
                for otro_ciudadano in self.ciudadanos:
                    if not otro_ciudadano.estado:  # Solo procesar ciudadanos enfermos
                        if ciudadano.tiene_contacto_fisico(otro_ciudadano):
                            if random.random() < self.enfermedad.infeccion_probable:
                                ciudadano.infectarse()
                                nuevos_infectados.append(ciudadano)
                                break  # Solo puede infectarse una vez por paso
            else:
                if random.random() < 1 / self.enfermedad.promedio_pasos:
                    ciudadano.estado = True
                    nuevos_recuperados.append(ciudadano)

        return nuevos_infectados, nuevos_recuperados

    def contar_estado(self):
        total_infectados = sum(not ciudadano.estado for ciudadano in self.ciudadanos)
        total_recuperados = sum(ciudadano.estado for ciudadano in self.ciudadanos)
        susceptibles = len(self.ciudadanos) - total_infectados - total_recuperados
        return total_infectados, total_recuperados, susceptibles
