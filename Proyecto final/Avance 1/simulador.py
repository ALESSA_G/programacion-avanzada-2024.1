import csv

class Simulador:
    def __init__(self, comunidad):
        self.comunidad = comunidad

    def run(self, pasos):
        with open('reporte_simulacion.csv', mode='w', newline='') as file:
            writer = csv.writer(file)
            writer.writerow(['Paso', 'Infectados', 'Recuperados', 'Susceptibles'])

        for i in range(pasos):
            nuevos_infectados, nuevos_recuperados = self.comunidad.simular_paso()
            infectados_actuales, recuperados_actuales, susceptibles_actuales = self.comunidad.contar_estado()
            
            print(f"Paso {i+1}: Infectados = {infectados_actuales}, Recuperados = {recuperados_actuales}, Susceptibles = {susceptibles_actuales}")
            self.generar_reporte_csv(i+1, infectados_actuales, recuperados_actuales, susceptibles_actuales)

    def generar_reporte_csv(self, paso, infectados, recuperados, susceptibles):
        try:
            with open('reporte_simulacion.csv', mode='a', newline='') as file:
                writer = csv.writer(file)
                writer.writerow([paso, infectados, recuperados, susceptibles])
        except Exception as e:
            print(f"Error al generar reporte CSV: {e}")
