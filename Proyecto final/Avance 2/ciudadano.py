class Ciudadano:
    
    def __init__(self, comunidad, _id, nombre, apellido, familia, enfermedad, estado=True, recuperado=False):
        self.comunidad = comunidad
        self._id = _id
        self.nombre = nombre
        self.apellido = apellido
        self.familia = familia
        self.enfermedad = enfermedad
        self.estado = estado
        self.recuperado = recuperado
