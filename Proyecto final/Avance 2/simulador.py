import csv
import random

class Simulador:
    def __init__(self):
        self.comunidad = None
    
    def set_comunidad(self, comunidad):
        self.comunidad = comunidad
    
    def run(self, pasos):
        for paso in range(pasos):
            self.simular_paso()
            self.guardar_estado_csv(paso)
    
    def simular_paso(self):
        nuevos_infectados = []
        for ciudadano in self.comunidad.ciudadanos:
            if ciudadano.estado and not ciudadano.enfermedad and not ciudadano.recuperado:
                for otro_ciudadano in self.comunidad.ciudadanos:
                    if otro_ciudadano.enfermedad and otro_ciudadano.estado:
                        if random.random() < self.comunidad.probabilidad_conexion_fisica: #Conexión física entre ciudadanos#
                            if random.random() < self.comunidad.enfermedad.infeccion_probable: #Conexión física a infección según probabilidad#
                                nuevos_infectados.append(ciudadano)
                                break
        
        for infectado in nuevos_infectados:
            infectado.enfermedad = self.comunidad.enfermedad
            infectado.estado = False  # Infectado y no sano.
            self.comunidad.num_infectados += 1
    
    def guardar_estado_csv(self, paso):
        with open(f'estado_dia_{paso+1}.csv', mode='w', newline='') as csvfile:
            fieldnames = ['id', 'nombre', 'apellido', 'familia', 'estado', 'enfermedad', 'recuperado']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            for ciudadano in self.comunidad.ciudadanos:
                writer.writerow({
                    'id': ciudadano._id,
                    'nombre': ciudadano.nombre,
                    'apellido': ciudadano.apellido,
                    'familia': ciudadano.familia,
                    'estado': 'Sano' if ciudadano.estado else 'Infectado' if ciudadano.enfermedad else 'Recuperado',
                    'enfermedad': 'Si' if ciudadano.enfermedad else 'No',
                    'recuperado': 'Si' if ciudadano.recuperado else 'No'
                })
