import csv
import random
from ciudadano import Ciudadano

class Comunidad:
    def __init__(self, num_ciudadanos, promedio_conexion_fisica, enfermedad, num_infectados, probabilidad_conexion_fisica):
        self.num_ciudadanos = num_ciudadanos
        self.promedio_conexion_fisica = promedio_conexion_fisica
        self.enfermedad = enfermedad
        self.num_infectados = num_infectados
        self.probabilidad_conexion_fisica = probabilidad_conexion_fisica
        self.ciudadanos = self.crear_ciudadanos()
    
    def crear_ciudadanos(self):
        ciudadanos = []
        with open('ciudadanos.csv', newline='') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                ciudadano = Ciudadano(
                    comunidad=self,
                    _id=row['id'],
                    nombre=row['nombre'],
                    apellido=row['apellido'],
                    familia=row['familia'],
                    enfermedad=None
                )
                ciudadanos.append(ciudadano)
        
        # Selecciona ciudadanos iniciales infectados aleatoriamente.
        infectados_iniciales = random.sample(ciudadanos, self.num_infectados)
        for infectado in infectados_iniciales:
            infectado.enfermedad = self.enfermedad
            infectado.estado = False  # Infectado y no sano.
        
        return ciudadanos
