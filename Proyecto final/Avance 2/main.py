from enfermedad import Enfermedad
from comunidad import Comunidad
from simulador import Simulador

def main():
    # Parámetros de la enfermedad.
    infeccion_probable = 0.3
    promedio_pasos = 5
    covid = Enfermedad(infeccion_probable=infeccion_probable, promedio_pasos=promedio_pasos)
    
    # Parámetros de la comunidad.
    num_ciudadanos = 20
    promedio_conexion_fisica = 6
    num_infectados = 10
    probabilidad_conexion_fisica = 0.8
    talca = Comunidad(
        num_ciudadanos=num_ciudadanos,
        promedio_conexion_fisica=promedio_conexion_fisica,
        enfermedad=covid,
        num_infectados=num_infectados,
        probabilidad_conexion_fisica=probabilidad_conexion_fisica
    )
    
    sim = Simulador()
    sim.set_comunidad(comunidad=talca)
    pasos_simulacion = 4
    sim.run(pasos=pasos_simulacion)

if __name__ == "__main__":
    main()
