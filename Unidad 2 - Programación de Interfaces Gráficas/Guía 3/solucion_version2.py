# Gabriela Sepúlveda - Programación Avanzada - 2024.1

import gi
gi.require_version('Gtk', '4.0')
from gi.repository import Gtk, Gio

APPID = 'com.gabriela.sepulveda.treeview'

# Datos iniciales para el ListStore.
data = [
    (1, 'Dato 1', 'Numero 1'),
    (2, 'Dato 2', 'Numero 2'),
    (3, 'Dato 3', 'Numero 3'),
]

class Gtk4TestTest(Gtk.ApplicationWindow):

    def __init__(self, app):
        Gtk.Window.__init__(
            self, application=app, title='Gtk.TreeView Test',
        )

        # Box principal.
        box = Gtk.Box(
            orientation=Gtk.Orientation.VERTICAL, spacing=20,
            margin_top=20, margin_bottom=20,
            margin_start=20, margin_end=20
        )

        # Barra de encabezado.
        header_bar = Gtk.HeaderBar.new()
        self.set_titlebar(titlebar=header_bar)
        self.set_title("Guía 3 - Programación Avanzada")

        # Menú en la barra de encabezado.
        menu = Gio.Menu.new()

        # Crear un popover para el menú.
        self.popover = Gtk.PopoverMenu()
        self.popover.set_menu_model(menu)

        # Botón del menú popover.
        self.menu_popover = Gtk.MenuButton()
        self.menu_popover.set_popover(self.popover)
        self.menu_popover.set_icon_name("open-menu-symbolic")
        header_bar.pack_end(self.menu_popover)

        # Acción del menú "Acerca de".
        about_menu = Gio.SimpleAction.new("about", None)
        about_menu.connect("activate", self.show_about_dialog)
        self.add_action(about_menu)
        menu.append("Acerca de", "win.about")

        # Entradas de texto para nuevos datos.
        self.entry_no = Gtk.Entry(placeholder_text="N°")
        self.entry_data = Gtk.Entry(placeholder_text="Dato")
        self.entry_number = Gtk.Entry(placeholder_text="Número")
        box.append(self.entry_no)
        box.append(self.entry_data)
        box.append(self.entry_number)

        # Botones de agregar y limpiar datos.
        add_button = Gtk.Button(label="Agregar")
        add_button.connect("clicked", self.on_add_button_clicked)
        header_bar.pack_start(add_button)

        clear_button = Gtk.Button(label="Limpiar")
        clear_button.connect("clicked", self.on_clear_button_clicked)
        header_bar.pack_start(clear_button)

        save_button = Gtk.Button(label="Guardar")
        save_button.connect("clicked", self.on_save_button_clicked)
        header_bar.pack_start(save_button)

        # ListStore para almacenar los datos del TreeView.
        self.liststore = Gtk.ListStore(int, str, str)
        for data_ref in data:
            self.liststore.append(list(data_ref))

        # TreeView para mostrar los datos.
        treeview = Gtk.TreeView(model=self.liststore, vexpand=True)
        treeview.connect('cursor_changed', self.on_cursor_changed)

        # Define las columnas del TreeView.
        for i, column_title in enumerate(['N°', 'Dato', 'Numero']):
            renderer = Gtk.CellRendererText()
            column = Gtk.TreeViewColumn(title=column_title)
            column.pack_start(renderer, False)
            column.add_attribute(renderer, "text", i)
            treeview.append_column(column)

        box.append(treeview)
        self.set_child(box)

    # Muestra el diálogo "Acerca de".
    def show_about_dialog(self, action, param):
        about_dialog = Gtk.AboutDialog(transient_for=self, modal=True)
        about_dialog.set_program_name("Guía 3 - Programación Avanzada")
        about_dialog.set_version("Versión 1.0")
        about_dialog.set_comments("Solución al ejemplo con GTK 4.0")
        about_dialog.set_copyright("© 2024 Gabriela Sepúlveda")
        about_dialog.present()

    # Selección de fila en el TreeView.
    def on_cursor_changed(self, treeview):
        treeselection = treeview.get_selection()
        model, iter = treeselection.get_selected()
        if iter:
            print(
                model.get_value(iter, 0),
                model.get_value(iter, 1),
                model.get_value(iter, 2))

    # Agrega datos al ListStore.
    def on_add_button_clicked(self, button):
        no = self.entry_no.get_text()
        data = self.entry_data.get_text()
        number = self.entry_number.get_text()
        if no and data and number:
            self.liststore.append([int(no), data, number])
            self.entry_no.set_text('')
            self.entry_data.set_text('')
            self.entry_number.set_text('')

    # Evento para limpiar todos los datos del ListStore
    def on_clear_button_clicked(self, button):
        self.liststore.clear()

    # Guarda los datos del ListStore en un archivo.
    def on_save_button_clicked(self, button):
        print("Guardar botón ha sido presionado.")
        try:
            with open('liststore_data.txt', 'w') as f:
                for row in self.liststore:
                    f.write(f"{row[0]}, {row[1]}, {row[2]}\n")
            print("Datos guardados correctamente.")
        except Exception as e:
            print(f"Error al guardar los datos: {e}")

class Gtk4TestApp(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self, application_id=APPID)

    def do_activate(self):
        window = Gtk4TestTest(self)
        window.present()

def main():
    app = Gtk4TestApp()
    app.run()

if __name__ == '__main__':
    main()
