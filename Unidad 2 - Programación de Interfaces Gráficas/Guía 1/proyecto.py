# Nombre: Gabriela Sepúlveda Rojas 
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

class MiApp(Gtk.Application):
    def __init__(self):
        super().__init__()

    # Implementamos el método do_activate:
    def do_activate(self):
        # Creación de la ventana principal.
        self.win = Gtk.ApplicationWindow(application=self)
        self.win.set_default_size(300, 200)
        self.win.set_title("Mi Aplicación")

        # Creación de contenedor Gtk.Box, el cual se añade a la ventana principal.
        box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        self.win.add(box)

        # Creción de un Gtk.Label con un mensaje y se añade al Gtk.Box.
        label = Gtk.Label(label="Por favor, ingrese el texto:")
        box.pack_start(label, True, True, 0)

        # Creacción de Gtk.Entry para que el usuario ingrese texto y se añade al Gtk.Box.
        self.entry = Gtk.Entry()
        box.pack_start(self.entry, True, True, 0)

        # Creción de Gtk.Button para guardar el contenido y se añade al Gtk.Box.
        button = Gtk.Button(label="Guardar")
        # Se conecta el evento clicked del botón al método guardar_contenido.
        button.connect("clicked", self.guardar_contenido)
        box.pack_start(button, True, True, 0)
        self.win.show_all()

    # Método para guardar el contenido ingresado en un archivo de texto:
    def guardar_contenido(self, button):
        # Capturamos el texto ingresado en el Gtk.Entry.
        texto = self.entry.get_text()
        
        # Cuadro de diálogo para seleccionar ubicación donde guardar el archivo.
        dialog = Gtk.FileChooserDialog(
            title="Guardar archivo",
            parent=self.win,
            action=Gtk.FileChooserAction.SAVE,
            buttons=(Gtk.STOCK_SAVE, Gtk.ResponseType.OK, Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL)
        )

        # Se ejecuta el diálogo y captura la respuesta del usuario.
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            # Si el usuario presionó "Guardar", se abrirá una nueva página para nombrar el documento.
            filename = dialog.get_filename()
            with open(filename, 'w') as file:
                file.write(texto)
            dialog.destroy()
            # Muestra un mensaje de éxito.
            self.mostrar_exito()
        else:
            # Si el usuario presionó "Cancelar", se destruye el diálogo.
            dialog.destroy()

    # Método para mostrar el mensaje de éxito:
    def mostrar_exito(self):
        # Creación de cuadro de diálogo.
        dialog = Gtk.MessageDialog(
            transient_for=self.win,
            flags=0,
            message_type=Gtk.MessageType.INFO,
            buttons=Gtk.ButtonsType.OK,
            text="Contenido guardado con éxito"
        )
        # Se crea el diálogo y se destruye después de que el usuario presione "OK".
        dialog.run()
        dialog.destroy()

# Bloque principal para ejecutar la aplicación
if __name__ == "__main__":
    app = MiApp()
    app.run()