# Gabriela Sepúlveda - Programación Avanzada - 2024.1

import sys
import gi
gi.require_version('Gtk', '4.0')
from gi.repository import Gio, Gtk, Gdk, GdkPixbuf, GLib
from rdkit import Chem
from rdkit.Chem import Descriptors, Draw

# Función para manejar la acción de salir.
def on_quit_action(_action):
    quit()

# Clase para la ventana principal.
class MainWindow(Gtk.ApplicationWindow):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Box principal.
        self.main_box = Gtk.Box.new(Gtk.Orientation.VERTICAL, 6)
        self.set_child(self.main_box)

        # Barra de encabezado.
        header_bar = Gtk.HeaderBar.new()
        self.set_titlebar(titlebar=header_bar)
        self.set_title("Visualizador de archivos mol")

        # Menú en la barra de encabezado.
        menu = Gio.Menu.new()
        self.popover = Gtk.PopoverMenu()
        self.popover.set_menu_model(menu)
        self.menu_popover = Gtk.MenuButton()
        self.menu_popover.set_popover(self.popover)
        self.menu_popover.set_icon_name("open-menu-symbolic")
        header_bar.pack_end(self.menu_popover)

        # Acciones del menú.
        about_menu = Gio.SimpleAction.new("about", None)
        about_menu.connect("activate", self.show_about_dialog)
        self.add_action(about_menu)
        menu.append("Acerca de", "win.about")

        action = Gio.SimpleAction.new("quit", None)
        action.connect("activate", on_quit_action)
        self.add_action(action)
        menu.append("Salir", "win.quit")

        # Botón para abrir archivos.
        boton_abrir = Gtk.Button()
        boton_abrir.set_label("Abrir")
        boton_abrir.connect("clicked", self.on_clicked_abrir_archivo)
        header_bar.pack_start(boton_abrir)

        # Menú para seleccionar acciones.
        self.dropdown_store = Gio.ListStore()
        self.dropdown = Gtk.DropDown(model=self.dropdown_store)
        self.main_box.append(self.dropdown)

        # Etiqueta para mostrar información.
        self.info_label = Gtk.Label()
        self.main_box.append(self.info_label)

        # Imagen de la molécula.
        self.image = Gtk.Image()
        self.main_box.append(self.image)

        self.dropdown.connect("notify::selected-item", self.on_dropdown_selected)

    # Método para mostrar el diálogo "Acerca de".
    def show_about_dialog(self, action, param):
        about_dialog = Gtk.AboutDialog(transient_for=self)
        about_dialog.set_program_name('Visualizador de archivos mol')
        about_dialog.set_version('Versión 1.0')
        about_dialog.set_authors(['Gabriela Sepulveda'])
        about_dialog.present()

    # Método para abrir un archivo.
    def on_clicked_abrir_archivo(self, widget):
        dialog = Gtk.FileChooserDialog(title="Abrir archivo", parent=self, action=Gtk.FileChooserAction.OPEN)
        dialog.add_button("Abrir", Gtk.ResponseType.OK)
        dialog.add_button("Cancelar", Gtk.ResponseType.CANCEL)

        # Filtrar solo archivos .mol.
        filter_mol = Gtk.FileFilter()
        filter_mol.set_name('Archivos MOL')
        filter_mol.add_pattern('*.mol')
        dialog.add_filter(filter_mol)

        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            file_path = dialog.get_file().get_path()
            self.load_molecule(file_path)
        dialog.destroy()

    # Método para cargar la molécula desde el archivo.
    def load_molecule(self, file_path):
        mol = Chem.MolFromMolFile(file_path)
        if mol:
            self.display_molecule_info(mol)
            self.display_molecule_image(mol)
        else:
            print("No se pudo cargar la molécula.")

    # Método para mostrar la información de la molécula seleccionada.
    def display_molecule_info(self, mol):
        formula = Chem.rdMolDescriptors.CalcMolFormula(mol)
        weight = Descriptors.MolWt(mol)
        rings = Chem.rdMolDescriptors.CalcNumRings(mol)

        # Limpiar y agregar elementos al menú.
        self.dropdown_store.remove_all()
        self.dropdown_store.append("Fórmula Molecular")
        self.dropdown_store.append("Peso Molecular")
        self.dropdown_store.append("Número de Anillos")

        # Almacenar información de la molécula.
        self.molecule_info = {
            "Fórmula Molecular": formula,
            "Peso Molecular": weight,
            "Número de Anillos": rings
        }

    # Método para mostrar la imagen de la molécula.
    def display_molecule_image(self, mol):
        img = Draw.MolToImage(mol, size=(300, 300))
        buf = GLib.Bytes.new(img.tobytes())
        pixbuf = GdkPixbuf.Pixbuf.new_from_bytes(buf, GdkPixbuf.Colorspace.RGB, False, 8, img.width, img.height, img.width * 3)
        self.image.set_from_pixbuf(pixbuf)

    # Método para manejar la selección en el menú desplegable.
    def on_dropdown_selected(self, dropdown, _):
        selected_action = dropdown.get_selected_item().get_string()
        if selected_action:
            info = self.molecule_info.get(selected_action, "")
            self.info_label.set_text(f"{selected_action}: {info}")

class MyApp(Gtk.Application):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    # Método para activar la aplicación.
    def do_activate(self):
        active_window = self.props.active_window
        if active_window:
            active_window.present()
        else:
            self.win = MainWindow(application=self)
            self.win.present()

app = MyApp()
app.run(sys.argv)